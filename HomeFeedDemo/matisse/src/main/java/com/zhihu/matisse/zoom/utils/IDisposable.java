package com.zhihu.matisse.zoom.utils;

public interface IDisposable {

    void dispose();
}
