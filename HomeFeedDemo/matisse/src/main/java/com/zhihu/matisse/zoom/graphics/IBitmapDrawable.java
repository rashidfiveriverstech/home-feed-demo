package com.zhihu.matisse.zoom.graphics;

import android.graphics.Bitmap;

import com.zhihu.matisse.zoom.ImageViewTouchBase;

/**
 * Base interface used in the {@link ImageViewTouchBase} view
 *
 * @author alessandro
 */
public interface IBitmapDrawable {

    Bitmap getBitmap();
}
