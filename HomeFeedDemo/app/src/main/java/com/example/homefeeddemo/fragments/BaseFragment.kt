package com.example.homefeeddemo.fragments

import android.content.Context
import android.os.Bundle

open class BaseFragment : androidx.fragment.app.Fragment() {
    val ARGS_INSTANCE = "com.frt.ballogyapp"
    internal lateinit var mFragmentNavigation: FragmentNavigation
    protected var countItems = 0
    protected var loading = true
    protected var pastVisiblesItems: Int = 0
    protected var visibleItemCount: Int = 0
    protected var totalItemCount: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is FragmentNavigation) {
            mFragmentNavigation = context
        }
    }

    interface FragmentNavigation {
        fun pushFragment(fragment: androidx.fragment.app.Fragment)
    }

}