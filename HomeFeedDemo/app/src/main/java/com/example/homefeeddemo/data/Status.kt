package com.example.homefeeddemo.data

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}