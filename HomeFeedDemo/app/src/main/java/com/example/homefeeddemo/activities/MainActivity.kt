package com.example.homefeeddemo.activities

import android.content.Intent
import android.database.Observable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.example.homefeeddemo.R
import com.example.homefeeddemo.models.SignInResponse
import com.example.homefeeddemo.rest.APIs
import com.example.homefeeddemo.rest.ApiClient
import com.example.homefeeddemo.viewmodel.HomePostViewModel

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
    fun login(view: View) {
        callApi()
    }

    private fun callApi() {

        val api: APIs = ApiClient.getClient()!!.create(APIs::class.java)
        var call: Call<SignInResponse> = api.login("android@gmail.com", "aaaaaa", "idhf8h10h0014841")

        call.enqueue(object: Callback<SignInResponse>{
            override fun onFailure(call: Call<SignInResponse>, t: Throwable) {
                Toast.makeText(this@MainActivity, "Failed", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(
                call: Call<SignInResponse>,
                response: Response<SignInResponse>
            ) {
                Toast.makeText(this@MainActivity, "Success", Toast.LENGTH_SHORT).show()
                ApiClient.myToken = response.body()?.data?.accessToken.toString()
                val intent = Intent (this@MainActivity, HomeFeedActivity::class.java)
                startActivity(intent)
            }

        })
    }
}
