package com.example.homefeeddemo.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.homefeeddemo.HomePostRepository
import com.example.homefeeddemo.data.Resource
import com.example.homefeeddemo.models.CreatePostResponse
import com.example.homefeeddemo.models.NetState
import com.example.homefeeddemo.models.PostsResponse
import okhttp3.RequestBody

class HomePostViewModel(application: Application) : AndroidViewModel(application) {

    val onStateUpdatedLiveData = MutableLiveData<NetState>()
    private val myRepository: HomePostRepository = HomePostRepository
    private val postsMutableLiveData = MutableLiveData<PostsResponse>()
    private var postObserver: Observer<PostsResponse?>
    private var createPostLiveData = MutableLiveData<Resource<CreatePostResponse>>()

    fun loadHomePostFeeds(userId: String?, groupId: Int?, limit: String, offset: String) {
        onStateUpdatedLiveData.value = NetState.LOADING
        myRepository.loadHomePostFeeds(userId, groupId, limit, offset, onStateUpdatedLiveData)
    }

    fun getPostData(): PostsResponse? {
        return postsMutableLiveData.value
    }


    init {
        Log.d("HomeFragment", "init HomePostViewModel called.. ")
        postObserver = Observer { post ->
            run {
                Log.d("HomeFragment", "PostObserver called.. ")
                if (post?.previous != null) {
                    if (!getPostData()?.data.isNullOrEmpty()) {
                        getPostData()?.data?.let { post.data?.addAll(0, it) }
                    }
                }
                postsMutableLiveData.value = post
            }
        }

        myRepository.getPostsMutableLiveData().observeForever(postObserver)
    }


    fun setCreatePostLiveDataRepo(createPostData: Resource<CreatePostResponse>?, mapCreatePost: HashMap<String, RequestBody>?) {
        return myRepository.setCreatePostData(createPostData, mapCreatePost)
    }

    fun getCreatePostLiveDataRepo(): MutableLiveData<Resource<CreatePostResponse>> {
        return myRepository.getCreatePostLiveData()
    }
    fun createPostLiveData(postData: Map<String, RequestBody>) {
        createPostLiveData = myRepository.createPost(postData)
    }
}
