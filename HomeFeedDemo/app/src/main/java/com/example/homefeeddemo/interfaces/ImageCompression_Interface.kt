package com.example.homefeeddemo.interfaces

import android.graphics.Bitmap

interface ImageCompression_Interface {

    fun imageCompressionSuccessfull(compressedImagePath: String)

    fun imageCompressionFailed(errorMessage: String?)

}