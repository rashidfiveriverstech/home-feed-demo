package com.example.homefeeddemo

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.gif.GifDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.homefeeddemo.models.PostData
import com.example.homefeeddemo.models.PostGroup
import com.example.homefeeddemo.models.PostResponseData
import com.example.homefeeddemo.models.PostShareBy
import com.example.homefeeddemo.utils.GeneralUtils
import de.hdodenhof.circleimageview.CircleImageView
import kotlin.collections.ArrayList

class HomePostAdapter(
    private val context: Context
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var postDataList: ArrayList<PostResponseData> = ArrayList()
    private var loadItem = true

    private var glideRequests: RequestManager? = null
    var thumbnailRequest: RequestBuilder<GifDrawable>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if (glideRequests == null) {
            glideRequests = Glide.with(context)
        }
        if (thumbnailRequest == null) {
            thumbnailRequest = Glide.with(context).asGif()
                .load(R.drawable.ic_volume_on_icon).decode(GifDrawable::class.java)
        }
        val groupInfoHolder = GroupInfoHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.home_general_post_item, parent, false),
            glideRequests
        )
        return groupInfoHolder
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        Log.d("TAGG", "On bind two parts")
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: List<Any>) {

        val data: PostResponseData? = postDataList[holder.adapterPosition]
        val groupData: PostData? = data?.postData
        val postGroup: PostGroup? = data?.group
        val postShareByUser: PostShareBy? = data?.shareBy
        if (getItemViewType(position) == GENERAL_POST) {
            val groupInfoHolder: GroupInfoHolder? = holder as (GroupInfoHolder)
            if (groupInfoHolder != null) {

                if (payloads.isNullOrEmpty() || (payloads.get(0).toString() != "[post_update]" &&
                            payloads.get(0).toString() != "[like_update]")) {
                    groupInfoHolder.ivImageThumbnail.visibility = View.GONE
                    groupInfoHolder.ivImage.visibility = View.GONE
                }

                groupData?.let { it1 ->

                    val admin =  data.admin
                    var imagePath = ""
                    var title = ""
                    if(admin != null && admin){
                        data.group?.coverImage?.let { imagePath = it }
                        data.group?.title?.let { title = it }
                    }else{
                        postShareByUser?.imagePath?.let { imagePath = it }
                        postShareByUser?.username?.let { title = it}
                    }
                    groupInfoHolder.tvUserName.text = title

                    Glide.with(context)
                        .load(imagePath)
                        .centerCrop()
                        .placeholder(R.drawable.ic_volume_off_icon)
                        .into(groupInfoHolder.ivUserImage)

                    if (!it1.media.isNullOrEmpty()) {
                        it1.media?.let {
                            if (!it.isNullOrEmpty()) {

                                it[0].media?.let { it3 ->

                                    it[0].type?.let { it2 ->
                                        if (groupInfoHolder.clLikeCommment.layoutParams is ConstraintLayout.LayoutParams) {
                                            val layoutParams: ConstraintLayout.LayoutParams =
                                                groupInfoHolder.clLikeCommment.layoutParams as ConstraintLayout.LayoutParams
                                            layoutParams.topMargin = GeneralUtils.dpToPx(context, 12f)
                                            groupInfoHolder.clLikeCommment.layoutParams = layoutParams
                                        }

                                        var heightVal = it1.media?.get(0)?.height?:0
                                        var widthVal = it1.media?.get(0)?.width?:0
                                        val screenWidth = context.resources?.displayMetrics?.widthPixels?:0
                                        if (it2 == "image" || it3.endsWith(".gif") || it3.endsWith(".png")) {

                                            holder.mediaContainer.setBackgroundColor(Color.WHITE)
                                            holder.mediaContainer.visibility = View.GONE
                                            groupInfoHolder.ivImage.alpha = 0f
                                            groupInfoHolder.ivImage.visibility = View.VISIBLE
                                            groupInfoHolder.ivImageThumbnail.visibility = View.VISIBLE

                                            val constraintParams =  groupInfoHolder.ivImage.getLayoutParams() as (ConstraintLayout.LayoutParams)
//                                            val constraintParams1 =  groupInfoHolder.ivImageThumbnail.getLayoutParams() as (ConstraintLayout.LayoutParams)
                                            if(widthVal > 0 && widthVal > screenWidth){
                                                widthVal = screenWidth
                                            }else {
                                                widthVal = screenWidth
                                            }
                                            constraintParams.width= widthVal
                                            constraintParams.height = widthVal
//                                            constraintParams1.width= widthVal
//                                            constraintParams1.height = widthVal
                                            groupInfoHolder.ivImage.layoutParams = constraintParams
//                                            groupInfoHolder.ivImageThumbnail.layoutParams = constraintParams1
                                            holder.ivImage.invalidate()
//                                            holder.ivImageThumbnail.invalidate()

                                            Log.d("TAGURL", "Url is $it3")
                                            if (!it3.endsWith("gif")) {
                                                glideRequests!!.load(it3).thumbnail(0.1f).listener(object : RequestListener<Drawable> {
                                                    override fun onLoadFailed(
                                                        e: GlideException?, model: Any?,
                                                        target: Target<Drawable>?, isFirstResource: Boolean
                                                    ): Boolean {
                                                        Log.d("TAGD", "onLoadFailed...")
                                                        return false
                                                    }

                                                    override fun onResourceReady(
                                                        resource: Drawable?, model: Any?, target: Target<Drawable>?,
                                                        dataSource: DataSource?, isFirstResource: Boolean
                                                    ): Boolean {
                                                        Log.d("TAGD", "onResourceReady...")
                                                        groupInfoHolder.ivImageThumbnail.visibility = View.GONE
                                                        groupInfoHolder.ivImage.alpha = 1f
                                                        groupInfoHolder.ivImage.scaleType =
                                                            ImageView.ScaleType.CENTER_CROP
                                                        return false
                                                    }

                                                }).into(groupInfoHolder.ivImage)

                                            } else {
                                                glideRequests!!.asGif().load(it3)
                                                    .listener(object : RequestListener<GifDrawable> {
                                                        override fun onLoadFailed(
                                                            e: GlideException?, model: Any?,
                                                            target: Target<GifDrawable>?, isFirstResource: Boolean
                                                        ): Boolean {
                                                            Log.d("TAGD", "onLoadFailed...")
                                                            return false
                                                        }

                                                        override fun onResourceReady(
                                                            resource: GifDrawable?,
                                                            model: Any?,
                                                            target: Target<GifDrawable>?,
                                                            dataSource: DataSource?,
                                                            isFirstResource: Boolean
                                                        ): Boolean {
                                                            Log.d("TAGD", "onResourceReady...")
                                                            groupInfoHolder.ivImageThumbnail.visibility = View.GONE
                                                            groupInfoHolder.ivImage.alpha = 1f
                                                            groupInfoHolder.ivImage.scaleType =
                                                                ImageView.ScaleType.CENTER_CROP
                                                            return false
                                                        }

                                                    }).into(groupInfoHolder.ivImage)
                                            }

                                        } else if (it2 == "video") {
                                            // Load Thumbnail
                                            holder.mediaContainer.setBackgroundColor(Color.BLACK)
                                            holder.backView.visibility = View.VISIBLE
                                            if(heightVal.equals(widthVal)){

                                                val frameParams =  holder.mediaThumbnail.getLayoutParams() as (FrameLayout.LayoutParams)
                                                val constraintParams =  holder.mediaContainer.getLayoutParams() as (ConstraintLayout.LayoutParams)
                                                widthVal = screenWidth
                                                frameParams.width= widthVal
                                                frameParams.height = widthVal
                                                constraintParams.width= widthVal
                                                constraintParams.height = widthVal
                                                holder.mediaThumbnail.layoutParams = frameParams
                                                holder.mediaThumbnail.invalidate()
                                                holder.mediaContainer.layoutParams = constraintParams
                                                holder.mediaContainer.invalidate()

                                            } else if(heightVal > widthVal){
                                                if(heightVal > 0){
                                                    val frameParams =  holder.mediaThumbnail.getLayoutParams() as (FrameLayout.LayoutParams)
                                                    val constraintParams =  holder.mediaContainer.getLayoutParams() as (ConstraintLayout.LayoutParams)
//                                                    var tempHeight :Double = heightVal * 1.0
//                                                    val tHeight = context.resources.displayMetrics.heightPixels
//                                                    val heightRect = tHeight - (tHeight/3)
//                                                    if(heightVal >= heightRect){
//                                                        tempHeight = heightRect*1.0
//                                                    }

                                                    frameParams.width= FrameLayout.LayoutParams.WRAP_CONTENT
                                                    frameParams.height = screenWidth
                                                    frameParams.gravity = Gravity.CENTER_HORIZONTAL
                                                    constraintParams.width= ConstraintLayout.LayoutParams.MATCH_PARENT
                                                    constraintParams.height = screenWidth
                                                    holder.mediaContainer.layoutParams = constraintParams
                                                    holder.mediaContainer.invalidate()
                                                    holder.mediaThumbnail.layoutParams = frameParams
                                                    holder.mediaThumbnail.invalidate()
                                                    holder.mediaThumbnail.adjustViewBounds =  true
                                                }

                                            }else{
                                                if(heightVal > 0){
                                                    val frameParams =  holder.mediaThumbnail.getLayoutParams() as (FrameLayout.LayoutParams)
                                                    val constraintParams =  holder.mediaContainer.getLayoutParams() as (ConstraintLayout.LayoutParams)
                                                    val videoWidth :Double = widthVal + 0.0
                                                    var heightProportion : Double = heightVal / (widthVal*1.0)
                                                    val tempHeight = heightProportion * screenWidth
                                                    frameParams.width= widthVal
                                                    frameParams.height = tempHeight.toInt()
                                                    constraintParams.width= widthVal
                                                    constraintParams.height = tempHeight.toInt()
                                                    holder.mediaThumbnail.layoutParams = frameParams
                                                    holder.mediaThumbnail.invalidate()
                                                    holder.mediaContainer.layoutParams = constraintParams
                                                    holder.mediaContainer.invalidate()
                                                }
                                            }

                                            holder.mediaThumbnail.visibility = View.VISIBLE
                                            Glide.with(context)
                                                .load(groupData?.media?.get(0)?.thumbnail)
                                                .into(groupInfoHolder.mediaThumbnail)
                                            holder.ivImage.visibility = View.GONE
                                            holder.ivImageThumbnail.visibility = View.GONE
                                            holder.mediaContainer.visibility = View.VISIBLE

                                            if (!payloads.isNullOrEmpty() && payloads[0].equals("post_update")) {
                                                Log.d("TAGG", " loaditem Not_load")
                                            } else {
                                                Log.d("TAGG", " loaditem $loadItem")
                                            }

                                        } else {
                                        }
                                    }
                                }
                            }
                        }
                    } else {

                        Log.d("mediaThumbnail: ", "1")
                        holder.thumbnail.visibility = View.GONE
                        holder.mediaThumbnail.visibility = View.GONE
                        holder.mediaContainer.visibility = View.GONE
                    }
                }


            }
        } else {

        }
    }




    override fun getItemCount(): Int {
        if (postDataList.isNullOrEmpty()) {
            return 0
        }
        return postDataList.size
    }

    fun getAdapterItem(position: Int): PostResponseData? {
        postDataList.let { if (position < it.size) return it[position] }
        return null
    }

    override fun getItemViewType(position: Int): Int {
        return GENERAL_POST
    }

    fun getItemList(): ArrayList<PostResponseData> {
        return postDataList
    }


    fun setItemsList(postList: ArrayList<PostResponseData>?) {
        postDataList.clear()
        postList?.let { postDataList.addAll(it) }
        notifyDataSetChanged()
    }

    fun setLoadItem(loadItem: Boolean) {
        this.loadItem = loadItem
        Log.d("TAGG", "set Load Item " + this.loadItem)
    }


    fun removeSpecifiedItem(position: Int){
        if(position < postDataList.size){
            postDataList.removeAt(position)
        }
    }

    class GroupInfoHolder(view: View, var requestManager: RequestManager?) : RecyclerView.ViewHolder(view) {

        var ivUserImage: CircleImageView = view.findViewById(R.id.cv_user_image)
        var tvUserName: AppCompatTextView = view.findViewById(R.id.tv_username)
        var tvPostText: AppCompatTextView = view.findViewById(R.id.tv_post_text)
        var ivImageThumbnail: ProgressBar = view.findViewById(R.id.iv_image_thumbnail)
        var ivImage: AppCompatImageView = view.findViewById(R.id.iv_image)
        var ivMore: AppCompatTextView = view.findViewById(R.id.iv_more_icon)
        var ivLike: AppCompatImageView = view.findViewById(R.id.iv_like)
//        var ivComment: AppCompatImageView = view.findViewById(R.id.iv_comment)
        var tvCommentOne: AppCompatTextView = view.findViewById(R.id.tv_comment_one)
        var tvCommentTwo: AppCompatTextView = view.findViewById(R.id.tv_comment_two)
        var tvLikeCount: AppCompatTextView = view.findViewById(R.id.tv_like_count)
        var tvViewLike: AppCompatTextView = view.findViewById(R.id.tv_view_like)
        var tvViewCommentLike: AppCompatTextView = view.findViewById(R.id.tv_view_comment)
        var tvCommentcount: AppCompatTextView = view.findViewById(R.id.tv_comment_count)
        var thumbnail: ProgressBar = view.findViewById(R.id.thumbnail)
        var ivVolumeControl: AppCompatImageView = view.findViewById(R.id.volume_control)
        var backView: View = view.findViewById(R.id.backView)
        var mediaContainer: FrameLayout = view.findViewById(R.id.media_container)
        var mediaThumbnail: AppCompatImageView = view.findViewById(R.id.iv_video_thumbnail)
        var clLikeCommment: ConstraintLayout = view.findViewById(R.id.layoutLikeComment)
        var tvTimeStamp: AppCompatTextView = view.findViewById(R.id.tvTimeStamp)
        var ivVerified: AppCompatImageView = view.findViewById(R.id.iv_verified)
        //        var progressBar: ProgressBar = view.findViewById(R.id.progressbar)
        var parent: View = view

        init {
            parent.tag = this
        }
    }



    companion object {
        private val GENERAL_POST = 1
    }

}
