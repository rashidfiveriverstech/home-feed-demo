package com.example.homefeeddemo.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.homefeeddemo.HomePostAdapter
import com.example.homefeeddemo.R
import com.example.homefeeddemo.models.PostsResponse
import com.example.homefeeddemo.viewmodel.HomePostViewModel
import kotlinx.android.synthetic.main.activity_home_feed.*

class HomeFeedActivity : AppCompatActivity() {

    private lateinit var homePostViewModel: HomePostViewModel
    private var layoutManager: LinearLayoutManager? = null
    private var adapter: HomePostAdapter? = null
    private lateinit var feedObserver: Observer<PostsResponse>
    private val handler = Handler(Looper.getMainLooper())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_feed)


        homePostViewModel = ViewModelProviders.of(this).get(HomePostViewModel::class.java)

        // 477 = android@gmail.com
        homePostViewModel.loadHomePostFeeds("477", 0, "10", "0")
        prepareDataSet()


        feedObserver = Observer<PostsResponse> {
            bindToPosts(it)
            Toast.makeText(this@HomeFeedActivity, "Inside Observer", Toast.LENGTH_SHORT).show()
        }

        fab_create_post.setOnClickListener {
            val intent = Intent(this@HomeFeedActivity, CreatePostActivity::class.java)
            startActivity(intent)
        }
    }
    private fun bindToPosts(response: PostsResponse?) {
        Log.d("TAG", "bindToPosts called...")
        if (response != null && !response.data.isNullOrEmpty()) {
            adapter?.setItemsList(response.data)
            rv_feed?.setMediaObjects(adapter?.getItemList())
            adapter?.let {
                if (it.itemCount <= 10) {
                    handler.postDelayed({
                        if (layoutManager?.findFirstCompletelyVisibleItemPosition() == 0) {
                            rv_feed?.setPlayPosition(-1)
                        }
                        rv_feed?.playVideo(false)
                    }, 500)
                }
            }
        } else {
            rv_feed?.setMediaObjects(ArrayList())
            adapter?.setItemsList(ArrayList())
        }
    }
    private fun prepareDataSet() {
        layoutManager = LinearLayoutManager(baseContext, RecyclerView.VERTICAL, false)
        layoutManager?.isItemPrefetchEnabled = true
        layoutManager?.initialPrefetchItemCount = 6
        rv_feed.layoutManager = layoutManager

        adapter = HomePostAdapter(this@HomeFeedActivity)

        rv_feed.adapter = adapter
        rv_feed.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                Log.d("TAG", "onScrolled called ...")
                if (layoutManager != null) {
                    val visibleItemCount = layoutManager!!.getChildCount()
                    val totalItemCount = layoutManager!!.getItemCount()
                    val pastVisibleItems = layoutManager!!.findFirstVisibleItemPosition()
                    if (pastVisibleItems + visibleItemCount >= totalItemCount && totalItemCount > 9) {
                        Log.d("TAG", "onScrolled called ...Loading data")
                        val myUid = 477
                        val postData = homePostViewModel.getPostData()

                        postData?._next?.offset?.let {
                            homePostViewModel.loadHomePostFeeds(
                                "477",
                                0, "10", it.toString()
                            )
                        }
                    }
                }

            }
        })
    }
}
