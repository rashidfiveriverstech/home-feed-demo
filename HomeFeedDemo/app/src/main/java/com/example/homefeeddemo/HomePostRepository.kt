package com.example.homefeeddemo

import androidx.lifecycle.MutableLiveData
import com.example.homefeeddemo.data.Resource
import com.example.homefeeddemo.models.CreatePostResponse
import com.example.homefeeddemo.models.NetState
import com.example.homefeeddemo.models.PostsResponse
import com.example.homefeeddemo.rest.APIs
import com.example.homefeeddemo.rest.ApiClient
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


object HomePostRepository {

    private var postsMutableLiveData = MutableLiveData<PostsResponse?>()
    private var createPostLiveData = MutableLiveData<Resource<CreatePostResponse>>()
    private var createPostMapLiveData = MutableLiveData<HashMap<String, RequestBody>>()

    fun loadHomePostFeeds(userId: String?, groupId: Int?, limit: String, offset: String,  onStateUpdatedLiveData : MutableLiveData<NetState> ) {

        val api: APIs = ApiClient.getClient()!!.create(APIs::class.java)
        var call: Call<PostsResponse>? = null

        call = api.getHomeFeedPosts("social_service", limit, offset, "public", "true", "false")
        call?.enqueue(object : Callback<PostsResponse> {
            override fun onResponse(call: Call<PostsResponse>?, response: Response<PostsResponse>?) {
                val postResponse = response?.body()
                if (response != null && response.isSuccessful && response.code() == 200 && postResponse != null &&
                    postResponse.code != 401 ) {
                    onStateUpdatedLiveData.postValue(NetState.LOADED)
                    val items = response.body()?.data?.map { it } ?: emptyList()
                    postsMutableLiveData.setValue(response.body())

                } else {
                    var message = ""
                    response?.message()?.let { message = it }
                    var code = -1
                    postResponse?.code?.let { code = it }
                    onStateUpdatedLiveData.postValue(NetState(NetState.Status.ERROR, message, code))
                }
            }

            override fun onFailure(call: Call<PostsResponse>?, t: Throwable?) {
                onStateUpdatedLiveData.postValue(t?.message?.let { NetState(NetState.Status.ERROR, it) })
            }
        })
    }


    fun getPostsMutableLiveData(): MutableLiveData<PostsResponse?> {
        return postsMutableLiveData
    }

    fun setCreatePostData(createPostData : Resource<CreatePostResponse>?, mapCreatePost: HashMap<String, RequestBody>?){
        createPostLiveData.value = createPostData
        createPostMapLiveData.value = mapCreatePost
    }
    fun getCreatePostLiveData() : MutableLiveData<Resource<CreatePostResponse>>{
        return createPostLiveData
    }

    fun createPost(postData: Map<String, RequestBody>): MutableLiveData<Resource<CreatePostResponse>> {

        val api: APIs = ApiClient.getClient()!!.create(APIs::class.java)
        val createPostCall = api.createPost(postData)
        createPostCall.enqueue(object : Callback<CreatePostResponse> {
            override fun onFailure(call: Call<CreatePostResponse>, t: Throwable) {
                createPostLiveData.value = Resource.error("Something went wrong " , null)
            }

            override fun onResponse(call: Call<CreatePostResponse>, response: Response<CreatePostResponse>) {
                if (response != null && response.isSuccessful && response.body() != null && (response.body()?.code == 200 || response.body()?.code == 201)) {
                    createPostLiveData.value = Resource.success(response.body())
                } else {
                    createPostLiveData.value = Resource.error(response.body()?.message?:"Something went wrong..", null)
                }
            }
        })
        return createPostLiveData

    }
}
