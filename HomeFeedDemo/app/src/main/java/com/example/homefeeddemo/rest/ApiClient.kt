package com.example.homefeeddemo.rest

import android.util.Log
import com.example.homefeeddemo.VideoPlayerRecyclerView
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.upstream.DefaultAllocator
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiClient {
    companion object
    {

        var mLoadControl: DefaultLoadControl? = null
        var volumeValue: VideoPlayerRecyclerView.VolumeState = VideoPlayerRecyclerView.VolumeState.OFF
        var myToken = ""
        private var client: OkHttpClient? = null
        val API_VERSION = "v3.0/"
        val BASE_URL = "https://staging.ballogy.com/api/$API_VERSION"
        var retrofit: Retrofit? = null


        fun getLoadControl(): DefaultLoadControl? {
            if (mLoadControl == null) { // LoadControl that controls when the MediaSource buffers more media, and how much media is buffered.
// LoadControl is injected when the player is created.
                val builder = DefaultLoadControl.Builder()
                builder.setAllocator(DefaultAllocator(true, 2 * 1024 * 1024))
                builder.setBufferDurationsMs(2000, 3000, 2000, 2000)
                builder.setPrioritizeTimeOverSizeThresholds(true)
                mLoadControl = builder.createDefaultLoadControl()
            }
            return mLoadControl
        }


        fun getClient(): Retrofit? {

            buildClient()

            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build()
            }
            return retrofit
        }

        private fun buildClient() {

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY

            client = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor { chain ->
                    val original = chain.request()
                    val response: Response
                    Log.e("sendToken", "true>>>>>>>>>>>>>>>>>>>>>>>>>>>")
                    val request: Request
                    val token = myToken
                    request = if (token != null && !token.isEmpty()) {
                        Log.d("TAG", "intercept: AFTER AUTHENTICATION")
                        original.newBuilder() //                                    .header("Authorization", "Token " + "d08349355baf540a000dc705bcaee5c5f204c986*.*QL6QByWOuZ3mMyWAKDJLvsRrI3x2ryrGet7K7UxO+Is=")//Dev Test
                            //                                .header("Authorization", "Token " + "4346303fca9258748e3cd4be47a685624f9288ef*.*59U4hqTvLkmKkJ76JxHLFo9ddRhAO1WtoERR4MsYiWzi4RuXQ1ywvCoUSPVifCgD")//Dev IOS Test
                            //                                .header("Authorization", "Token " + "0550e5c41cb1ba217586efeff96ed00fb596a12c*.*rUTXHAVTqSOY8LiNmnv3fT9VfBm3yQdliLqJvy5UwZF0WXJF8hwQTdaa34ppojbi") // Android
                            .header("Authorization", "Token $token") // Android
                            //                                .header("Authorization", "Token " + "a79907f9b86e7f67d0eeea459b0ca414aa10da60*.*QL6QByWOuZ3mMyWAKDJLvsRrI3x2ryrGet7K7UxO+Is=") // Ballogy1
                            //                                .header("Authorization", "Token " + "cdcb133fc72480f3e439d5703e1990c7687c39f6*.*2GrSle6IUa55DPfX3Zo1Og==")
                            .method(original.method, original.body)
                            .build()
                    } else {
                        Log.d("TAG", "intercept: BEFORE AUTHENTICATION")
                        original.newBuilder().method(original.method, original.body)
                            .build()
                    }
                    response = chain.proceed(request)
                    response
                }
                .connectTimeout(80, TimeUnit.SECONDS)
                .writeTimeout(300, TimeUnit.SECONDS)
                .readTimeout(80, TimeUnit.SECONDS)
                .build()
        }
    }
}