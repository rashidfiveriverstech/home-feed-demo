package com.example.homefeeddemo.data

enum class MediaTypes {
    VIDEO,
    PHOTO,
    GIF
}